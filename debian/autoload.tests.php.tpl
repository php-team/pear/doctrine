<?php

// Require
require_once 'Doctrine/ORM/autoload.php';

// Suggest

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                'doctrine\\tests\\models\\ddc3597\\ddc3597image' => '/../tests/Tests/Models/DDC3597/DDC3597Image.php',
                'doctrine\\tests\\models\\ddc4006\\ddc4006user' => '/../tests/Tests/Models/DDC4006/DDC4006User.php',
                'doctrine\\tests\\models\\enums\\product' => '/../tests/Tests/Models/Enums/Product.php',
                'doctrine\\tests\\orm\\mapping\\mappingdrivertestcase' => '/../tests/Tests/ORM/Mapping/MappingDriverTestCase.php',
                ___CLASSLIST___
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require ___BASEDIR___$classes[$cn];
        }
    },
    ___EXCEPTION___,
    ___PREPEND___
);
// @codeCoverageIgnoreEnd

// Files
