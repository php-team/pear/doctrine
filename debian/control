Source: doctrine
Section: php
Priority: optional
Maintainer: Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>
Uploaders: David Prévot <taffit@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-phpcomposer,
               help2man,
               php-doctrine-collections,
               php-doctrine-dbal,
               php-doctrine-inflector,
               php-doctrine-instantiator,
               php-doctrine-lexer,
               php-doctrine-persistence,
               php-sqlite3,
               php-symfony-cache,
               php-symfony-console,
               php-symfony-var-exporter,
               phpab,
               phpunit
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/php-team/pear/doctrine.git
Vcs-Browser: https://salsa.debian.org/php-team/pear/doctrine
Homepage: https://www.doctrine-project.org/projects/orm.html
Rules-Requires-Root: no

Package: php-doctrine-orm
Architecture: all
Depends: php-mysql | php-pgsql | php-sqlite3 | php-sybase,
         ${misc:Depends},
         ${phpcomposer:Debian-require}
Recommends: ${phpcomposer:Debian-suggest}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: doctrine, ${phpcomposer:Debian-provide}
Description: tool for object-relational mapping
 ${phpcomposer:description}.
 Doctrine 2 is an object-relational mapper (ORM) for PHP that provides
 transparent persistence for PHP objects. It sits on top of a powerful
 database abstraction layer (DBAL). One of its key features is the
 option to write database queries in a proprietary object oriented SQL
 dialect called Doctrine Query Language (DQL), inspired by Hibernates
 HQL. This provides developers with a powerful alternative to SQL that
 maintains flexibility without requiring unnecessary code duplication.
 .
 The Doctrine Project provides several libraries primarily focused on
 database storage and object mapping.
